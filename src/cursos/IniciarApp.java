package cursos;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Insets;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import java.awt.GridLayout;
import java.awt.Color;
import java.awt.SystemColor;


public class IniciarApp {

	public JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IniciarApp window = new IniciarApp();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public IniciarApp() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setForeground(SystemColor.windowText);
		frame.getContentPane().setBackground(new Color(135, 206, 235));
		frame.setResizable(false);
		frame.setBounds(100, 100, 370, 205);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblCadastroDeCursos = new JLabel("Configura\u00E7\u00F5es dos Cursos");
		lblCadastroDeCursos.setForeground(new Color(0, 0, 0));
		lblCadastroDeCursos.setBounds(0, 11, 364, 49);
		lblCadastroDeCursos.setHorizontalAlignment(SwingConstants.CENTER);
		lblCadastroDeCursos.setFont(new Font("Times New Roman", Font.PLAIN, 22));
		frame.getContentPane().add(lblCadastroDeCursos);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(173, 216, 230));
		panel.setBounds(74, 71, 227, 76);
		frame.getContentPane().add(panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JButton btnNewButton = new JButton("Cadastrar Curso");
		btnNewButton.setBackground(new Color(30, 144, 255));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Evento Cadastro Curso
				CadastrarCurso window = new CadastrarCurso();
				window.frame.setVisible(true);
				frame.dispose();
			}
		});
		panel.add(btnNewButton);
		
		JButton btnNewButton_3 = new JButton("Listar Cursos");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Evento Listar Cursos
				ListarCursos window = new ListarCursos();
				window.frame.setVisible(true);
				frame.dispose();
			}
		});
		btnNewButton_3.setBackground(new Color(30, 144, 255));
		panel.add(btnNewButton_3);
	}
}