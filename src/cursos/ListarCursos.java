package cursos;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ListarCursos {

	JFrame frame;

	Connection conn = null;
	Curso cursoSelecionado;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListarCursos window = new ListarCursos();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ListarCursos() {
		conn = sqliteConnection.dbConector();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(135, 206, 235));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("Nossos Cursos");
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 22));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(10, 0, 414, 27);
		frame.getContentPane().add(lblNewLabel);


		DefaultListModel listModel = new DefaultListModel();

		Departamento depart = new Departamento();
		ArrayList<Curso> lista = depart.buscarCursos(conn);
		System.out.println(lista.size());
//		for (int i = 0; i < lista.size(); i++) {
//			listModel.addElement(lista.get(i).getNomeCurso());
//		}

		final JList list_1 = new JList(lista.toArray());

		list_1.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
			//Valor clicado
				cursoSelecionado=(Curso)list_1.getSelectedValues()[0];
//				JOptionPane.showMessageDialog(null, list_1.getSelectedValues()[0]);
			}
		});
		list_1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list_1.setBounds(20, 38, 390, 162);
		frame.getContentPane().add(list_1);

		JButton btnNewButton = new JButton("Excluir");
		btnNewButton.setBounds(321, 216, 89, 23);
		frame.getContentPane().add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cursoSelecionado.deletar(conn);
				JOptionPane.showMessageDialog(null,"Curso "+cursoSelecionado.getNomeCurso()+ " excluido");
				ListarCursos window = new ListarCursos();
				window.frame.setVisible(true);
				frame.dispose();
	
			}
		});

		JButton btnNewButton_1 = new JButton("Atualizar");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CadastrarCurso window = new CadastrarCurso(cursoSelecionado);
				window.frame.setVisible(true);
				frame.dispose();
			}
		});
		btnNewButton_1.setBounds(211, 216, 89, 23);
		frame.getContentPane().add(btnNewButton_1);

		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IniciarApp window = new IniciarApp();
				window.frame.setVisible(true);
				frame.dispose();
			}
		});
		btnCancelar.setBounds(10, 216, 89, 23);
		frame.getContentPane().add(btnCancelar);
	}
}