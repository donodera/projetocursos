package cursos;
import java.sql.*;
import java.util.*;

public class Curso {
	
	//atributos
	
	private int cod_curso;
	private String nome_curso;
	private int num_vagas;
	private double valor_curso; 
	
	// constructor
	
	public Curso(){
		
	}
	public Curso(int cod_curso) {
		this.cod_curso = cod_curso;
	}
	public Curso( String nome_curso,int num_vagas, double valor_curso) {
		this.nome_curso= nome_curso;
		this.num_vagas= num_vagas;
		this.valor_curso= valor_curso;
	}
	
	//getters and setters
	
	public int getCodCurso() {
		return cod_curso;			
	}
	public String getNomeCurso() {
		return nome_curso;
	}
	
	public int getNumVagas() {
		return num_vagas;
	}
	
	public double getValorCurso(){
		return valor_curso;
	}
	
	public void setCodCurso(int cod_curso) {
		this.cod_curso = cod_curso;
	}
	public void setNomeCurso(String nome_curso) {
		this.nome_curso = nome_curso;
	}
	
	public void setNumVagas(int num_vagas) {
		this.num_vagas = num_vagas;
	}
	public void setValorCurso(double valor_curso) {
		this.valor_curso = valor_curso;
	}
//----------------------------INSERT----------------------------------------------------------	
	public void inserir(Connection conn) {
	      String sqlInsert = 
	         "INSERT INTO CURSO(nome_curso, num_vagas, valor_curso) VALUES(UPPER(?), ?, ?)";
	   
	      try (PreparedStatement stm = conn.prepareStatement(sqlInsert);) {
	         stm.setString(1, getNomeCurso());
	         stm.setInt(2, getNumVagas());
	         stm.setDouble(3, getValorCurso());
	         stm.execute();
	      } 
	      catch (Exception e) {
	         e.printStackTrace();
	         try {
	            conn.rollback();
	         } 
	         catch (SQLException e1) {
	            System.out.print(e1.getStackTrace());
	         }
	      } 
	   }
//--------------------------DELETE------------------------------------------------------------
	
	 public void deletar(Connection conn) {
	      String sqlDelete = "DELETE FROM CURSO WHERE nome_curso = ?";
	      try (PreparedStatement stm = conn.prepareStatement(sqlDelete);) {
	         stm.setString(1, getNomeCurso());
	      
	         stm.execute();
	      } 
	      catch (Exception e) {
	         e.printStackTrace();
	         try {
	            conn.rollback();
	         } 
	         catch (SQLException e1) {
	            System.out.print(e1.getStackTrace());
	         }
	      } 
	   }
//-------------------------READ-------------------------------------------------
	 
	 public void mostrarCurso(Connection conn) {
	      String sqlSelect = 
	         "SELECT * FROM curso WHERE nome_curso = ?";
	   
	      try (PreparedStatement stm = conn.prepareStatement(sqlSelect);){
	         stm.setString(1, getNomeCurso());
	         try (ResultSet rs = stm.executeQuery();){
	         /*este outro try e' necessario pois nao da' para abrir o resultset
	          *no anterior uma vez que antes era preciso configurar o parametro
	          *via setInt; se nao fosse, poderia se fazer tudo no mesmo try
	          */
	            if (rs.next()) {
	               this.setNumVagas(rs.getInt(2));
	               this.setValorCurso(rs.getDouble(3));
	            }
	         
	         } 
	         catch (Exception e) {
	            e.printStackTrace();
	         }
	      }
	      catch (SQLException e1) {
	         System.out.print(e1.getStackTrace());
	      }
	   } 
	 
	 
//---------------------------atualizar-----------------------------------------------------------------------
	
	 public void atualizar(Connection conn) {
	      String sqlUpdate = "UPDATE CURSO SET nome_curso = UPPER(?), num_vagas= ?, valor_curso =? WHERE cod_curso = ?";
	    
	      try (PreparedStatement stm = conn.prepareStatement(sqlUpdate);){
	         stm.setString(1, nome_curso);
	         stm.setInt(2, num_vagas);
	         stm.setDouble(3, valor_curso);
	         stm.setInt(4,cod_curso);
	      
	         stm.execute();
	      } 
	      catch (Exception e) {
	         e.printStackTrace();
	         try {
	            conn.rollback();
	         } 
	         catch (SQLException e1) {
	            System.out.print(e1.getStackTrace());
	         }
	      } 
	   }
	 
	 
	 
	 
	 
	 
	 
	   
	   public String toString() {
//	      return "Curso [Nome=" + nome_curso + ", Vagas=" + num_vagas + ", Valor="
//	         	+ valor_curso + "]";
		   
		   return nome_curso;
	   }

	   


		       
	

}	

