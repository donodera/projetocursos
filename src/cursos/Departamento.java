package cursos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;


public class Departamento {


	public ArrayList<Curso> buscarCursos(Connection conn){
		String sqlSelect = "SELECT cod_curso, nome_curso, num_vagas, valor_curso FROM curso";
		ArrayList<Curso> lista = new ArrayList<>();

		try(PreparedStatement stm = conn.prepareStatement(sqlSelect);
				ResultSet rs = stm.executeQuery();){
			while(rs.next()){
				Curso curso = new Curso();
				curso.setCodCurso(rs.getInt("cod_curso"));
				curso.setNomeCurso(rs.getString("nome_curso"));
				curso.setNumVagas(rs.getInt("num_vagas"));
				curso.setValorCurso(rs.getDouble("valor_curso"));
				lista.add(curso);
			}
		} catch(Exception e){
			e.printStackTrace();
		}
		return lista;
	}  
}
