package cursos;
import java.awt.EventQueue;
import java.sql.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import java.awt.event.ActionListener;

public class CadastrarCurso {
	Connection conn = null;
	JFrame frame;
	private JTextField txtName;
	private JTextField txtVagas;
	private JTextField txtValor;
	private Curso cursoSelecionado = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastrarCurso window = new CadastrarCurso();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CadastrarCurso() {
		conn = sqliteConnection.dbConector();
		initialize();
	}
	
	public CadastrarCurso(Curso cursoSelecionado) {
		this();
		this.cursoSelecionado = cursoSelecionado;
		txtName.setText(cursoSelecionado.getNomeCurso());
		txtVagas.setText("" + cursoSelecionado.getNumVagas());
		txtValor.setText(""+cursoSelecionado.getValorCurso());
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(135, 206, 235));
		frame.setBounds(100, 100, 439, 220);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblCadastrarDoCurso = new JLabel("Cadastrar/Alterar Curso");
		lblCadastrarDoCurso.setBounds(0, 0, 410, 27);
		lblCadastrarDoCurso.setFont(new Font("Times New Roman", Font.PLAIN, 22));
		lblCadastrarDoCurso.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(lblCadastrarDoCurso);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(173,216,230));
		panel.setBounds(10, 34, 400, 137);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNomeDoCurso = new JLabel("Nome do Curso");
		lblNomeDoCurso.setBounds(10, 5, 174, 14);
		panel.add(lblNomeDoCurso);
		
		txtName = new JTextField();
		txtName.setBounds(10, 22, 359, 20);
		panel.add(txtName);
		txtName.setColumns(10);
		
		txtVagas = new JTextField();
		txtVagas.setBounds(20, 71, 54, 20);
		panel.add(txtVagas);
		txtVagas.setColumns(10);
		
		JLabel lblNmeroDeVagas = new JLabel("N\u00FAmero de vagas");
		lblNmeroDeVagas.setBounds(10, 53, 100, 14);
		panel.add(lblNmeroDeVagas);
		
		JLabel lblValorDoCurso = new JLabel("Valor do Curso");
		lblValorDoCurso.setBounds(120, 53, 87, 14);
		panel.add(lblValorDoCurso);
		
		txtValor = new JTextField();
		txtValor.setBounds(132, 71, 53, 20);
		panel.add(txtValor);
		txtValor.setColumns(10);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ListarCursos window = new ListarCursos();
				window.frame.setVisible(true);
				frame.dispose();
			}
		});
		btnCancelar.setBounds(205, 102, 89, 23);
		panel.add(btnCancelar);
		btnCancelar.setBackground(new Color(204, 204, 204));
		
		JButton btnNewButton = new JButton("Salvar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(cursoSelecionado== null){
					Curso novo = new Curso(txtName.getText(), Integer.parseInt(txtVagas.getText()),Double.parseDouble(txtValor.getText()));
					novo.inserir(conn);
					JOptionPane.showMessageDialog(null, "Curso inserido com sucesso");
				}else {
					cursoSelecionado.setNomeCurso(txtName.getText());
					cursoSelecionado.setNumVagas(Integer.parseInt(txtVagas.getText()));
					cursoSelecionado.setValorCurso(Double.parseDouble(txtValor.getText()));
					cursoSelecionado.atualizar(conn);
					JOptionPane.showMessageDialog(null, "Curso atualizado com sucesso");
				}
				ListarCursos window = new ListarCursos();
				window.frame.setVisible(true);
				frame.dispose();
			}
		});
		btnNewButton.setBackground(new Color(204, 204, 204));
		btnNewButton.setBounds(304, 102, 89, 23);
		panel.add(btnNewButton);
		
		
		JLabel lblR = new JLabel("R$");
		lblR.setHorizontalAlignment(SwingConstants.RIGHT);
		lblR.setBounds(97, 74, 34, 17);
		panel.add(lblR);
	}
	
	
}